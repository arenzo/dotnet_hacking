// Autogenerated app on DotNet Core for Linux
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("This is another line");
            //Defining Variables:
            var integer = 10;
            Console.WriteLine($"My integer is: {integer}");
        }
    }
    class MyClass
    {
        static void Another(string[] args)
        {
            //Instantiate another Class
            var myClass = "This is another class!!";
            Console.WriteLine(myClass);
        }
    }
}
